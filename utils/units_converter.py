from _decimal import Decimal
from enum import Enum
from typing import NamedTuple


class MeasureUnit(Enum):
    IN = "in"
    CM = "cm"
    MM = "mm"
    PT = "pt"
    TWIP = "twip"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name})"


class LengthUnit(NamedTuple):
    value: Decimal
    unit: MeasureUnit

    def __str__(self):
        return f"{self.__class__.__name__}: {self._asdict().values()}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict().items()})>"


class UnitConverter:
    _conversion_to_in: dict[MeasureUnit, Decimal] = {
        MeasureUnit.IN: 1,
        MeasureUnit.CM: 2.54,
        MeasureUnit.MM: 25.4,
        MeasureUnit.PT: 72,
        MeasureUnit.TWIP: 1440
    }

    @classmethod
    def convert(cls, __from: LengthUnit, __to: MeasureUnit):
        _coeff: Decimal = Decimal(cls._conversion_to_in[__to] / cls._conversion_to_in[__from.unit])
        return int(_coeff * __from.value)
