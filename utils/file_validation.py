import faulthandler
from os import walk
from typing import Iterable, Union

from loguru import logger
from lxml import etree
# noinspection PyProtectedMember
from lxml.etree import XMLParser, _ElementTree, XMLSchema, XMLSyntaxError
from pathlib import Path

from docx_modify.init_logger import configure_custom_logging


class InvalidFileTypeError(TypeError):
    """The file has an invalid extension."""


def validate_path(path: Union[Path, str], extension: str) -> Path:
    try:
        path_file: Path = Path(path).resolve(strict=True)
    except OSError as e:
        logger.error(f"{e.__class__.__name__}, {e.strerror}")
        raise
    if not path_file.exists():
        logger.error(f"The path to {path_file} does not exist")
        raise FileNotFoundError
    if path_file.is_dir():
        logger.error(f"The path to {path_file} leads to the directory instead of the file")
        raise IsADirectoryError
    if not path_file.suffix.endswith(extension):
        logger.error(f"The file {path_file} is not an {extension.upper()} file")
        raise InvalidFileTypeError
    return path_file


def files_dir(path_dir: Path, extensions: Iterable[str] = None):
    if extensions is None:
        pass
    return [
        Path(dirpath).joinpath(f)
        for dirpath, _, filenames in walk(path_dir)
        for f in filenames]


class XMLFileValidator:
    def __init__(self, xsd_file: Union[Path, str]):
        self._xsd_file: Path = validate_path(xsd_file, "xsd")

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._xsd_file})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._xsd_file}"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._xsd_file == other.__class__
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._xsd_file != other.__class__
        else:
            return NotImplemented

    def __bool__(self):
        return True

    @property
    def element_tree(self) -> _ElementTree:
        return etree.parse(self._xsd_file)

    @property
    def _xml_schema(self) -> XMLSchema:
        return XMLSchema(self.element_tree)

    @property
    def _xml_parser(self) -> XMLParser:
        return XMLParser(schema=self._xml_schema, resolve_entities=False)

    def validate(self, path_dir: Path):
        extensions: tuple[str] = ("xml",)
        logger.error(f"files_dir = {files_dir(path_dir, extensions)}")
        for file in files_dir(path_dir, extensions):
            try:
                logger.info(f"The file {file.resolve()}")
                etree.parse(file, self._xml_parser)
            except XMLSyntaxError as e:
                logger.error(f"{e.__class__.__name__}, {e.position}, {e.args}")
            else:
                logger.info("Ok")
        return


@logger.catch
@configure_custom_logging("file_validation")
def main():
    path: Path = Path(__file__).parent.joinpath("validation/wml.xsd")
    path_dir: Path = Path(__file__).parent.parent.joinpath("sources").joinpath("typo")
    logger.error(files_dir(path_dir))
    xml_file_validator: XMLFileValidator = XMLFileValidator(path)
    xml_file_validator.validate(path_dir)


if __name__ == '__main__':
    faulthandler.enable()
    main()
