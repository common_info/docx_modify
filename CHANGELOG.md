# Versions #

## v1.3.0 ##

* Added archive formatting;
* Added a pop-up menu to choose the formatting modes;
* Changed the GUI library from tkinter to wxPython;

## v1.2.1 ##

* Added processing files with landscape pages;
* Added help information while running;

## v1.2.0 ##

* Added capability of processing several files;
* Added the inspection of file types;
* Restructured module architecture and file separating;
* Added content to the README.md file;

## v1.1.0 ##

* Formatted headers and footers;
* Added GUI elements to get user files and desired formatting mode;
* Generated executable files;

## v1.0.0 ##

* The main features are realized;
