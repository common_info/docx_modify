#!/usr/bin/python
# -*- coding: cp1251 -*-
import os.path
from importlib import import_module
from subprocess import CalledProcessError, run
from sys import executable, modules, version_info

from docx_modify.exceptions import InvalidPythonVersion

_parent_path: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


if version_info < (3, 8):
    print("������ Python ������ ���� �� ����� 3.8.")
    input("������� <Enter>, ����� ������� ���� ...")
    raise InvalidPythonVersion


for package in ("loguru", "lxml"):
    if package not in modules:
        try:
            import_module(package)
        except ModuleNotFoundError | ImportError | RuntimeError:
            try:
                run([executable, "-m", "pip", "install", package]).check_returncode()
            except CalledProcessError as e:
                print(f"{e.__class__.__name__}\n��� ������ {e.returncode}\n������ {e.output}")
                print(f"�� ������� ������������� ����� `{package}`")
                raise
            except OSError as e:
                print(f"{e.__class__.__name__}\n���� {e.filename}\n������ {e.strerror}")
                print(f"�� ������� ������������� ����� `{package}`")
                raise
        else:
            continue


log_folder: str = os.path.abspath(os.path.join(_parent_path, "logs/"))


if not os.path.exists(log_folder):
    os.mkdir(log_folder)


if __name__ == '__main__':
    from file_processing import run_script
    run_script()
