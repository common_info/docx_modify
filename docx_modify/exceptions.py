class FileNotInArchiveError(KeyError):
    """File is not found in the archive."""


class ZipFileZippedError(AttributeError):
    """Zip file is zipped."""


class ZipFileUnzippedError(AttributeError):
    """Zip file is unzipped."""


class CollectionItemNotFoundError(KeyError):
    """The file is not found in the Word file collection."""


class InvalidOrientationError(KeyError):
    """Unknown type of section orientation."""


class InvalidPythonVersion(BaseException):
    """The installed Python version is outdated."""


class WxUserInputAbortedError(Exception):
    """Работа программы прервана пользователем ..."""


class InvalidRadioBoxChoiceError(KeyError):
    """The value is not specified in the radiobox."""
