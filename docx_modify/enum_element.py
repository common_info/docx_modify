from enum import Enum
from pathlib import Path
from tkinter import IntVar
from tkinter.ttk import Radiobutton
from typing import Any, NamedTuple, Optional, Sequence

from lxml.etree import ElementBase

from docx_modify.core_elements.clark_name import fqdn
from docx_modify.xml_elements.xml_element_factory import new_xml


class DocumentMode(Enum):
    ARCHIVE = "archive"
    TYPO = "typo"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name})"


class SectionOrientation(Enum):
    """Document section orientation

    Available options: PORTRAIT, LANDSCAPE.
    """
    LANDSCAPE = "landscape"
    PORTRAIT = "portrait"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name})"


class SectionType(Enum):
    """Document section start type

    Available options: CONTINUOUS, NEWCOLUMN, NEWPAGE, EVENPAGE, ODDPAGE.
    """
    CONTINUOUS = "continuous"
    NEWCOLUMN = "newColumn"
    NEWPAGE = "newPage"
    EVENPAGE = "evenPage"
    ODDPAGE = "oddPage"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name})"


class SectionPgBorder(Enum):
    """Document section page border side

    Available options: TOP, BOTTOM, LEFT, RIGHT, INSIDEH, INSIDEV, TL2BR (top-left to bottom-right),
    TR2BL (top-right to bottom-left).
    """
    TOP = "top"
    BOTTOM = "bottom"
    LEFT = "left"
    RIGHT = "right"
    INSIDEH = "insideH"
    INSIDEV = "insideV"
    TL2BR = "tl2br"
    TR2BL = "tr2bl"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name})"


class SectionPgMar(NamedTuple):
    """Document section page margins

    Attributes:
        bottom (int): The distance from the bottom
        footer (int): The distance from the footer
        gutter (int): The distance from the gutter
        header (int): The distance from the header
        left (int): The distance from the left
        right (int): The distance from the right
        top (int): The distance from the top
    """
    bottom: int
    footer: int
    gutter: int
    header: int
    left: int
    right: int
    top: int

    def __str__(self):
        return f"{self.__class__.__name__}: {self._asdict().values()}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict().items()})>"

    @classmethod
    def typo_zero_section(cls):
        """Specifies the class instance for the zero section in the document."""
        return cls(284, 340, 0, 907, 1418, 624, 6804)

    @classmethod
    def typo_one_section(cls):
        """Specifies the class instance for the first section in the document."""
        return cls(4253, 340, 0, 907, 1418, 624, 992)

    @classmethod
    def typo_two_section(cls):
        """Specifies the class instance for the second section in the document."""
        return cls(284, 340, 0, 907, 1418, 624, 992)

    @classmethod
    def typo_landscape_section(cls):
        """Specifies the class instance for sections oriented as LANDSCAPE."""
        return cls(850, 340, 0, 1616, 1644, 709, 624)

    @classmethod
    def archive_zero_section(cls):
        return cls(284, 340, 0, 907, 1418, 624, 6804)

    @classmethod
    def archive_one_section(cls):
        return cls(1135, 55, 0, 426, 1418, 624, 992)

    @property
    def element(self) -> ElementBase:
        """Specifies the xml element

        Generates the object with all fields as attributes.

        Returns:
             ElementBase: The element to add to the XML file
        """
        element: ElementBase = new_xml("w:pgMar")
        for field in self._fields:
            element.set(fqdn(f"w:{field}"), f"{getattr(self, field)}")
        return element


class SectionPgSz(NamedTuple):
    """Document section page size

   Attributes:
       h (int): The page height
       w (int): The page width
       orient (str): The page orientation, portait or landscape
   """
    h: int
    w: int
    orient: str

    def __str__(self):
        return f"{self.__class__.__name__}: {self._asdict().values()}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict().items()})>"

    @classmethod
    def a4_portrait(cls):
        """Specifies the class instance for sections oriented as PORTRAIT."""
        return cls(16_838, 11_906, "portrait")

    @classmethod
    def a4_landscape(cls):
        """Specifies the class instance for sections oriented as LANDSCAPE."""
        return cls(11_906, 16_838, "landscape")

    @property
    def element(self) -> ElementBase:
        """Specifies the xml element

        Generates the object with all fields as attributes.

        Returns:
            ElementBase: The object to add to the XML file
            Does not add the 'w:orient' attribute if the value is 'portrait'.
        """
        element: ElementBase = new_xml("w:pgSz")
        for field in self._fields:
            if field == "orient" and self.orient == "portrait":
                continue
            else:
                element.set(fqdn(f"w:{field}"), f"{getattr(self, field)}")
        return element


class XmlRelationshipTarget(Enum):
    """Document relationship target

    Available options: TYPO_ZERO_FOOTER_XML, TYPO_DEFAULT_FOOTER_XML, TYPO_ONE_FOOTER_XML, TYPO_EVEN_FOOTER_XML,
    TYPO_ODD_FOOTER_XML, TYPO_FOOTER_LANDSCAPE_EVEN_XML, TYPO_FOOTER_LANDSCAPE_ODD_XML, TYPO_DEFAULT_HEADER_XML,
    TYPO_ONE_HEADER_XML, TYPO_HEADER_LANDSCAPE_EVEN_XML, TYPO_HEADER_LANDSCAPE_ODD_XML, ARCH_FIRST_HEADER_XML,
    ARCH_EVEN_HEADER_XML, ARCH_DEFAULT_HEADER_XML, ARCH_FIRST_FOOTER_XML, ARCH_EVEN_FOOTER_XML, ARCH_DEFAULT_FOOTER_XML.
    """
    TYPO_ZERO_FOOTER_XML = "footer1.xml"
    TYPO_DEFAULT_FOOTER_XML = "footer2.xml"
    TYPO_ONE_FOOTER_XML = "footer3.xml"
    TYPO_EVEN_FOOTER_XML = "footer4.xml"
    TYPO_ODD_FOOTER_XML = "footer5.xml"
    TYPO_FOOTER_LANDSCAPE_EVEN_XML = "footer6.xml"
    TYPO_FOOTER_LANDSCAPE_ODD_XML = "footer7.xml"
    TYPO_ONE_FOOTER_DEFENSE_XML = "footer8.xml"

    TYPO_DEFAULT_HEADER_XML = "header1.xml"
    TYPO_ONE_HEADER_XML = "header2.xml"
    TYPO_HEADER_LANDSCAPE_EVEN_XML = "header3.xml"
    TYPO_HEADER_LANDSCAPE_ODD_XML = "header4.xml"

    ARCH_FIRST_FOOTER_XML = "footer1.xml"
    ARCH_EVEN_FOOTER_XML = "footer2.xml"
    ARCH_DEFAULT_FOOTER_XML = "footer3.xml"

    ARCH_FIRST_HEADER_XML = "header1.xml"
    ARCH_EVEN_HEADER_XML = "header2.xml"
    ARCH_DEFAULT_HEADER_XML = "header3.xml"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"


class XmlRelationshipType(Enum):
    """Document relationship type."""
    FOOTER = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/footer"
    PACKAGE = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/package"
    HYPERLINK = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink"
    IMAGE = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image"
    THEME = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"
    ENDNOTES = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/endnotes"
    NUMBERING = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering"
    WEB_SETTINGS = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings"
    SETTINGS = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings"
    HEADER = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/header"
    STYLES = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"
    FONT_TABLE = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable"
    FOOTNOTES = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/footnotes"
    CUSTOMXML = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/customXml"
    OLE_OBJECT = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/oleObject"
    GLOSSARY = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/glossaryDocument"
    KEYMAP_CUSTOMIZATIONS = "http://schemas.microsoft.com/office/2006/relationships/keyMapCustomizations"
    OFFICE_DOCUMENT = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"
    CUSTOM_PROPERTIES = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/custom-properties"
    CUSTOM_XML = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/customXml"
    HDPHOTO = "http://schemas.microsoft.com/office/2007/relationships/hdphoto"
    CORE_PROPERTIES_PACKAGE = "http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"
    METADATA_CORE = "http://schemas.openxmlformats.org/officedocument/2006/relationships/metadata/core-properties"
    DIGITAL_SIGNATURE = "http://schemas.openxmlformats.org/package/2006/relationships/digital-signature/signature"
    DS_CERTIFICATE = "http://schemas.openxmlformats.org/package/2006/relationships/digital-signature/certificate"
    DIGITAL_SIGNATURE_ORIGIN = "http://schemas.openxmlformats.org/package/2006/relationships/digital-signature/origin"
    EXTENDED_PROPERTIES = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties"
    EXTENDED_PROPERTIES_PURL = "http://purl.oclc.org/ooxml/officeDocument/relationships/extendedProperties"
    THUMBNAIL = "http://schemas.openxmlformats.org/package/2006/relationships/metadata/thumbnail"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"


class XmlHdrFtrReference(Enum):
    """Document header and footer reference type

    Available options: DEFAULT, EVEN, FIRST.
    """
    DEFAULT = "default"
    EVEN = "even"
    FIRST = "first"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"


class XmlReference(Enum):
    """Document header or footer reference

    Available options: HEADER, FOOTER.
    """
    HEADER = "w:headerReference"
    FOOTER = "w:footerReference"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.value})>"


class DialogChoice(NamedTuple):
    choice: str
    value: int

    def __str__(self):
        return f"{self.__class__.__name__}: {self._asdict().values()}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict().items()})>"


class MessageDialogElement(NamedTuple):
    """Specifies message dialog choices.

    Attributes:
        message(str) : the window message to show
        title(str) : the window title to show
        choices(Sequence[DialogChoice]]) : the options to select from
        values(Sequence[Any]) : the values of the variable
    """
    message: str
    title: str
    choices: Sequence[DialogChoice]
    values: Sequence[Any]

    def __str__(self):
        return f"{self.__class__.__name__}: {self._asdict().values()}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict().items()})>"

    @classmethod
    def mode_dialog(cls):
        message: str = "Выберите форматы, которые необходимы:"
        title: str = "Форматы документов"
        choices: tuple[DialogChoice, ...] = (
            DialogChoice("Архивный формат", 0),
            DialogChoice("Типографский формат", 1),
            DialogChoice("Архивный + Типографский формат", 2),
        )
        values: tuple[list[DocumentMode], ...] = (
            [DocumentMode.ARCHIVE], [DocumentMode.TYPO], [DocumentMode.ARCHIVE, DocumentMode.TYPO]
        )
        return cls(message, title, choices, values)

    @classmethod
    def formatting_dialog(cls):
        message: str = "Выберите форматирование страниц:"
        title: str = "Форматирование страниц"
        choices: tuple[DialogChoice, ...] = (
            DialogChoice("Для односторонней печати", 0),
            DialogChoice("Для двусторонней печати", 1),
        )
        values: tuple[bool, ...] = (
            False, True
        )
        return cls(message, title, choices, values)

    @property
    def choice_items(self):
        return [item.choice for item in self.choices]

    def get_choice(self, value: int):
        for choice in self.choices:
            if choice.value == value:
                return choice
        else:
            raise KeyError


class UserInputValues:
    def __init__(
            self,
            document_modes: Optional[list[DocumentMode]] = None,
            path_files: Optional[list[Path]] = None,
            is_two_sided: Optional[bool] = None,
            is_defense_ministery: Optional[bool] = None):
        if document_modes is None:
            document_modes = []
        if path_files is None:
            path_files = []
        if is_two_sided is None:
            is_two_sided = False
        if is_defense_ministery is None:
            is_defense_ministery = False

        self._document_modes: list[DocumentMode] = document_modes
        self._path_files: list[Path] = path_files
        self._is_two_sided: bool = is_two_sided
        self._is_defense_ministery: bool = is_defense_ministery

    @property
    def __attrs(self):
        return "_document_modes", "_path_files", "_is_two_sided", "_is_defense_ministery"

    def to_dict(self):
        return {item: getattr(self, item) for item in self.__attrs}

    def __str__(self):
        return f"{self.__class__.__name__}: {self.to_dict().values()}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.to_dict().items()})>"

    def __len__(self):
        return len(self._path_files)

    def __bool__(self):
        return len(self) > 0

    def __hash__(self):
        return hash((self._document_modes, self._path_files, self._is_two_sided, self._is_defense_ministery))

    def __key(self):
        return self._document_modes, self._path_files, self._is_two_sided, self._is_defense_ministery

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() == other.__key()
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.__key() != other.__key()
        else:
            return NotImplemented

    @property
    def document_modes(self):
        return self._document_modes

    @property
    def path_files(self):
        return self._path_files

    @property
    def is_two_sided(self):
        return self._is_two_sided

    @property
    def is_defense_ministery(self):
        return self._is_defense_ministery

    @document_modes.setter
    def document_modes(self, value):
        self._document_modes = value

    @path_files.setter
    def path_files(self, value):
        self._path_files = value

    @is_two_sided.setter
    def is_two_sided(self, value):
        self._is_two_sided = value

    @is_defense_ministery.setter
    def is_defense_ministery(self, value):
        self._is_defense_ministery = value

    @property
    def is_none(self):
        if not (self._document_modes or self._path_files or self._is_two_sided or self._is_defense_ministery):
            return True
        return False


class RadioButton(NamedTuple):
    master: Any
    command: Any
    text: str
    variable: IntVar
    value: int
    style: str

    def __str__(self):
        return f"{self.__class__.__name__}: {self._asdict().values()}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict().items()})>"

    def to_radio_button(self):
        return Radiobutton(
            master=self.master,
            command=self.command,
            style=self.style,
            text=self.text,
            variable=self.variable,
            value=self.value)
