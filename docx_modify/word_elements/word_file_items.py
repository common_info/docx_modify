from pathlib import Path
from typing import Iterable, Optional

from loguru import logger

from docx_modify.core_elements.core_document import CoreZipFile
from docx_modify.word_elements.word_file import _WordFile, WordFileCollection


class WordFileHeaderFooter(_WordFile):
    @property
    def basic_file_path(self) -> Path:
        return self._path_dir.joinpath(f"sources/{self.add_path}/headers_footers")

    @property
    def _archive_folder(self) -> str:
        return "word"


class WordFileImage(_WordFile):
    def __init__(self, core_zip_file: CoreZipFile):
        self._name: str = "_logo_st.png"
        super().__init__(self._name, core_zip_file)

    @property
    def basic_file_path(self) -> Path:
        return self._path_dir.joinpath(f"sources/{self.add_path}/image")

    @property
    def _archive_folder(self) -> str:
        return "word/media"


class WordFileRels(_WordFile):
    @property
    def basic_file_path(self) -> Path:
        return self._path_dir.joinpath(f"sources/{self.add_path}/rels")

    @property
    def _archive_folder(self):
        return "word/_rels"


class WordFileRelsCollection(WordFileCollection):
    def set_word_files(self):
        path_dir: Path = self._path_dir.joinpath(f"sources/{self.add_path}/rels")
        _file_names: list[str] = [file.name for file in path_dir.iterdir()]
        return self._word_files.extend(WordFileRels(name, self._core_zip_file) for name in _file_names)

    def add_to_archive(self, file_names: Iterable[str] = None):
        if file_names is None:
            file_names: list[str] = self.file_names
        for file_name in file_names:
            word_file: _WordFile = self.__getitem__(file_name)
            name_from: Path = word_file.basic_xml_file
            name_to: Path = word_file.full_path_archive
            self._core_zip_file.copy(name_from, name_to)
            logger.debug(f"The file {name_from} is copied to {name_to}")
        return


class WordFileBasic(WordFileCollection):
    def set_word_files(self):
        path_dir: Path = self._path_dir.joinpath(f"sources/{self.add_path}/headers_footers")
        _file_names: list[str] = [file.name for file in path_dir.iterdir()]
        return self._word_files.extend([WordFileHeaderFooter(_name, self._core_zip_file) for _name in _file_names])

    def add_to_archive(self, file_names: Iterable[str] = None):
        if file_names is None:
            file_names: list[str] = self.file_names
        for file_name in file_names:
            word_file: _WordFile = self.__getitem__(file_name)
            name_from: Path = word_file.basic_xml_file
            name_to: Path = word_file.full_path_archive
            self._core_zip_file.copy(name_from, name_to)
            logger.debug(f"The file {name_from} is copied to {name_to}")
        return


class WordFileHeadersFooters(WordFileCollection):
    def _files_dir(self, path: Optional[Path] = None) -> list[Path]:
        if path is None:
            path: Path = self._path_dir.joinpath("word")
        return list(filter(lambda x: x.is_file, path.iterdir()))

    def set_word_files(self):
        return self.word_files.extend(
            [_WordFile(str(_file_name), self._core_zip_file) for _file_name in self._file_paths])

    @property
    def _file_paths(self) -> list[Path]:
        return list(filter(lambda x: x.name.startswith("footer") or x.name.startswith("header"), self._files_dir()))

    def _file_names(self) -> list[str]:
        return list(map(lambda x: f"word/{x.name}", self._file_paths))

    def delete_from_archive(self, file_names: Iterable[str] = None):
        if file_names is None:
            file_names: list[str] = self._file_names()
        for file_name in file_names:
            if file_name in self._core_zip_file.files:
                self._core_zip_file.delete(file_name)
        return


class WordFileHdrFtrRels(WordFileCollection):
    def _files_dir(self, path: Optional[Path] = None) -> list[Path]:
        if path is None:
            path: Path = self._path_dir.joinpath("word/_rels")
        return list(filter(lambda x: x.is_file, path.iterdir()))

    def set_word_files(self):
        return self.word_files.extend(
            [_WordFile(str(_file_name), self._core_zip_file) for _file_name in self._file_paths])

    @property
    def _file_paths(self) -> list[Path]:
        return list(filter(lambda x: x.name.startswith("footer") or x.name.startswith("header"), self._files_dir()))

    def _file_names(self) -> list[str]:
        return list(map(lambda x: f"word/_rels/{x.name}", self._file_paths))

    def delete_from_archive(self, file_names: Iterable[str] = None):
        if file_names is None:
            file_names: list[str] = self._file_names()
        for file_name in file_names:
            if file_name in self._core_zip_file.files:
                self._core_zip_file.delete(file_name)
        return
