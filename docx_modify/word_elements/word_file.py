from pathlib import Path
from typing import Iterable

from loguru import logger

from docx_modify.core_elements.core_document import CoreZipFile, UnzippedFile
from docx_modify.exceptions import CollectionItemNotFoundError


class _WordFile(UnzippedFile):
    @property
    def basic_file_path(self) -> Path:
        raise NotImplementedError

    @property
    def add_path(self):
        return self._core_zip_file.mode.value

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name}, {self._core_zip_file}, {self.basic_xml_file})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self.full_path}, {self.basic_xml_file}"

    @property
    def basic_xml_file(self) -> Path:
        return self.basic_file_path.joinpath(self._name)

    @property
    def full_path_archive(self) -> Path:
        return self._core_zip_file.path_dir.joinpath(self._archive_folder).joinpath(self._name)

    @property
    def _archive_folder(self):
        raise NotImplementedError

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._name == other._name
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._name != other._name
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self._name)

    def __add__(self, other):
        if isinstance(other, WordFileCollection):
            other.word_files.append(self)
            return other
        else:
            return NotImplemented

    __radd__ = __add__
    __iadd__ = __add__


class WordFileCollection:
    def __init__(self, core_zip_file: CoreZipFile, word_files: Iterable[_WordFile] = None):
        if word_files is None:
            word_files: list[_WordFile] = []
        self._core_zip_file: CoreZipFile = core_zip_file
        self._word_files: list[_WordFile] = [*word_files]
        self._path_dir: Path = core_zip_file.path_dir

    def __repr__(self):
        return f"<{self.__class__.__name__}({[repr(word_file) for word_file in self._word_files]})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {[word_file.name for word_file in self._word_files]}"

    @property
    def add_path(self):
        return self._core_zip_file.mode.value

    @property
    def word_files(self) -> list[_WordFile]:
        return self._word_files

    @word_files.setter
    def word_files(self, value):
        self._word_files = value

    @property
    def file_names(self) -> list[str]:
        return [word_file.name for word_file in self._word_files]

    def __iter__(self):
        return iter(self._word_files)

    def __contains__(self, item):
        return item in self._word_files

    def __getitem__(self, item):
        if isinstance(item, str):
            if item not in self.file_names:
                logger.error(f"Файл {item} не найден")
                raise CollectionItemNotFoundError
            item: int = self.file_names.index(item)
        return self._word_files.__getitem__(item)

    def __add__(self, other):
        if isinstance(other, _WordFile) or issubclass(other.__class__, _WordFile):
            self.word_files.append(other)
            return self
        else:
            return NotImplemented

    __radd__ = __add__
    __iadd__ = __add__
