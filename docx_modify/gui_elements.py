from __future__ import annotations

import os.path
from _tkinter import TclError
from pathlib import Path
from textwrap import dedent
from tkinter.constants import FLAT, LEFT, N, NW, S, TOP, W, X
from tkinter import IntVar, Tk
from tkinter.filedialog import askopenfilenames
from tkinter.font import Font
from tkinter.messagebox import askyesnocancel
from tkinter.ttk import Button, Frame, Label, Radiobutton, Style
from typing import Optional

from loguru import logger

from docx_modify.enum_element import DocumentMode, MessageDialogElement, RadioButton, UserInputValues


def null_command():
    pass


class ModeFrame(Frame):
    def __init__(self, app: App):
        super().__init__(app.root, border=5, relief=FLAT, padding=(5, 5, 5, 5))

        label: Label = Label(
            self,
            anchor=N,
            border=0,
            justify="center",
            padding=[5, 5, 5, 5],
            relief=FLAT,
            text="Выберите формат документов:",
            underline=0)
        label.pack(anchor=N, fill=X, side=TOP)

        def set_mode_option():
            if mode_int_var.get() == 0:
                app.user_input_values._document_modes = [DocumentMode.ARCHIVE]
            elif mode_int_var.get() == 1:
                app.user_input_values._document_modes = [DocumentMode.TYPO]
            elif mode_int_var.get() == 2:
                app.user_input_values._document_modes = [DocumentMode.ARCHIVE, DocumentMode.TYPO]
            self.destroy()

        mode_int_var: IntVar = IntVar()
        mode_int_var.set(0)

        _frame_buttons: Frame = Frame(self)

        message_dialog_element: MessageDialogElement = MessageDialogElement.mode_dialog()
        for text, value in message_dialog_element.choices:
            rb: Radiobutton = RadioButton(
                master=_frame_buttons,
                command=null_command,
                text=text,
                variable=mode_int_var,
                value=value,
                style="TRadiobutton").to_radio_button()
            rb.pack(anchor=N, side=TOP, ipadx=20, ipady=5)

        _frame_buttons.pack(fill=X, anchor=S)

        _frame_ok_cancel: Frame = Frame(self)

        ok_button: Button = Button(
            master=_frame_ok_cancel,
            command=set_mode_option,
            text="ОК",
            style="TButton",
            padding=[])
        ok_button.pack(padx=20, pady=20, anchor=W, side=LEFT)

        cancel_button: Button = Button(
            master=_frame_ok_cancel,
            command=app.cancel_command,
            text="Отмена",
            style="TButton")
        cancel_button.pack(padx=20, pady=20, anchor=W, side=LEFT)

        _frame_ok_cancel.pack(fill=X, anchor=S)

        for widget in self.winfo_children():
            widget.pack(padx=5, pady=5)

        self.pack()


class FormatFrame(Frame):
    def __init__(self, app: App):
        super().__init__(app.root, border=5, relief=FLAT, padding=(5, 5, 5, 5), name="check")

        label: Label = Label(
            self,
            anchor=N,
            border=0,
            justify="center",
            padding=[5, 5, 5, 5],
            relief=FLAT,
            text="Выберите форматирование страниц:",
            underline=0)
        label.pack(anchor=N, fill=X, side=TOP)

        def set_format_option():
            if format_int_var.get() == 0:
                app.user_input_values._is_two_sided = False
            elif format_int_var.get() == 1:
                app.user_input_values._is_two_sided = True
            self.destroy()

        format_int_var: IntVar = IntVar()
        format_int_var.set(0)

        message_dialog_element: MessageDialogElement = MessageDialogElement.formatting_dialog()

        _frame_buttons: Frame = Frame(self)

        for text, value in message_dialog_element.choices:
            rb: Radiobutton = RadioButton(
                master=_frame_buttons,
                command=null_command,
                text=text,
                variable=format_int_var,
                value=value,
                style="TRadiobutton").to_radio_button()
            rb.pack(side="left", anchor=NW, padx=20, pady=20)

        _frame_buttons.pack(fill=X, anchor=S)

        _frame_ok_cancel: Frame = Frame(self)

        ok_button: Button = Button(
            master=_frame_ok_cancel,
            command=set_format_option,
            text="ОК",
            style="TButton")
        ok_button.pack(anchor=W, padx=20, pady=20, side=LEFT)

        cancel_button: Button = Button(
            master=_frame_ok_cancel,
            command=app.cancel_command,
            text="Отмена",
            style="TButton")
        cancel_button.pack(anchor=W, padx=20, pady=20, side=LEFT)

        for widget in self.winfo_children():
            widget.pack(padx=5, pady=5)

        self.pack()


def create_file_dialog():
    filetypes: tuple[tuple[str, str], ...] = (("Word files", "*.docx "),)
    initialdir: str = os.path.join(os.path.expanduser("~"), "Desktop")
    title: str = "Выберите файлы для обработки:"

    return askopenfilenames(filetypes=filetypes, initialdir=initialdir, title=title)


def create_message_box():
    title: str = "Определение типа поставки"
    message: str = dedent("""Документы для поставки Министерству обороны?

Yes: Да --- поставка для МО;
No: Нет --- иная поставка;      
Cancel: Отмена --- прервать работу программы и выйти без изменения файлов.
""")
    icon: str = "question"

    return askyesnocancel(title=title, message=message, icon=icon)


class DefenseMinisteryFrame(Frame):
    def __init__(self, app: App):
        super().__init__(app.root)

        title: str = "Определение типа поставки"
        message: str = dedent("""Документы для поставки Министерству обороны?

Yes: Да --- поставка для МО;
No: Нет --- иная поставка;      
Cancel: Отмена --- прервать работу программы и выйти без изменения файлов.
""")
        icon: str = "question"

        message_box: Optional[bool] = askyesnocancel(title=title, message=message, icon=icon)

        if message_box is None:
            app.cancel_command()
        else:
            app.user_input_values.is_defense_ministery = message_box
        self.destroy()


class App:
    def cancel_command(self):
        logger.info("Работа программы прервана пользователем ...")
        self._root.destroy()
        self._root.quit()

    def __init__(self, root: Tk):
        self._root: Tk = root
        self._root.title = "Настройка работы"
        # self._root.geometry = "250x150"
        self._user_input_values: Optional[UserInputValues] = UserInputValues()

        _radiobutton_font: Font = Font(
            family="Times",
            name="TRadiobutton",
            size=16,
            weight="bold",
            slant="roman",
            underline=False,
            overstrike=False)
        s: Style = Style()
        s.configure("TRadiobutton", font=_radiobutton_font)
        _button_font: Font = Font(
            family="Times",
            name="TButton",
            size=14,
            weight="normal",
            slant="roman",
            underline=False,
            overstrike=False)
        s.configure("TButton", font=_button_font)
        _label_font: Font = Font(
            family="Times",
            name="TLabel",
            size=18,
            weight="bold",
            slant="roman",
            underline=False,
            overstrike=False
        )
        s.configure("TLabel", font=_label_font)

        try:
            mode_frame: ModeFrame = ModeFrame(self)
            mode_frame.pack()
            mode_frame.wait_window()

            format_frame: FormatFrame = FormatFrame(self)
            format_frame.pack()
            format_frame.wait_window()

            self._root.withdraw()

            file_dialog = create_file_dialog()
            if not file_dialog:
                self.cancel_command()
            else:
                self._user_input_values.path_files = [Path(path) for path in file_dialog]

            message_box: Optional[bool] = create_message_box()
            if message_box is None:
                self.cancel_command()
            else:
                self._user_input_values.is_defense_ministery = message_box

            logger.error(self._user_input_values)
            self._root.destroy()
            self._root.quit()
        except TclError as e:
            logger.debug(f"{e.__class__.__name__}: {e.args}")
            self._user_input_values = None
            self._root.quit()

    @property
    def root(self):
        return self._root

    @property
    def user_input_values(self):
        return self._user_input_values


def get_user_input() -> Optional[UserInputValues]:
    """Gets the user input and converts to the FileItem instances

    Returns:
        list[FileItem] | None: The FileItem objects if not aborted
    """
    root: Tk = Tk()
    app: App = App(root)
    app.root.mainloop()

    return app.user_input_values
