from itertools import product
from pathlib import Path
from textwrap import dedent
from time import sleep
from typing import Union

from loguru import logger

from docx_modify.core_elements.core_document import CoreDocument, CoreDocumentMode, CoreZipFile
from docx_modify.enum_element import DocumentMode, SectionOrientation, UserInputValues
# from docx_modify.gui_elements import get_user_input
from docx_modify.init_logger import configure_custom_logging
from docx_modify.main import log_folder
from docx_modify.word_elements.word_file_items import WordFileBasic, WordFileHdrFtrRels, WordFileHeadersFooters, \
    WordFileImage, WordFileRelsCollection
from docx_modify.xml_elements.xml_document import XmlDocument
from docx_modify.xml_elements.xml_hdr_ftr import HdrFtrReference, HdrFtrRelRefControllerMode,  \
    HdrFtrRelReferenceController
from docx_modify.xml_elements.xml_relationships import XmlRelationship, XmlRelationships
from docx_modify.xml_elements.xml_section import XmlSection, XmlSectionArch, XmlSectionTypo
from docx_modify.xml_elements.xml_settings import XmlSettings
from docx_modify.xml_elements.xml_styles import XmlStyles


def show_prompt():
    """Specifies and outputs the prompt in the console."""
    _help: str = dedent("""--------------------
Краткий алгоритм:

1. В открывшемся окне выберите файлы, которые необходимо изменить.
Можно указать несколько файлов. Все не docx-файлы игнорируются.

2. В открывшемся окне уточните форматирование файлов: требуется ли форматирование для двусторонней печати.

Доступные кнопки:
Yes --- да, применить форматирование для двусторонней печати (зеркальное);
No --- нет, применить форматирование для односторонней печати;
Cancel --- отменить, остановить работу программы.

При форматировании для двусторонней печати рамки на обеих сторонах листа совпадают.
При форматировании для односторонней печати рамки на всех страницах имеют одинаковые отступы.

3. В обработанном документе обновите поля со значением "ОБНОВИТЬ", а также номер страницы в нижнем колонтитуле.

4. В обработанном документе заполните поля со значением "ЗАПОЛНИТЬ".
--------------------
""")
    print(_help)
    sleep(1)


def _core_preprocessing(path: Path, document_mode: DocumentMode):
    core_document: CoreDocument = CoreDocument(path)
    core_document_mode: CoreDocumentMode = core_document.make_document(document_mode)
    core_document_mode.duplicate()
    core_zip_file: CoreZipFile = CoreZipFile(path, document_mode)
    core_zip_file.unarchive()
    return core_zip_file


def _word_files_processing(core_zip_file: CoreZipFile):
    word_file_headers_footers: WordFileHeadersFooters = WordFileHeadersFooters(core_zip_file)
    word_file_headers_footers.set_word_files()
    word_file_headers_footers.delete_from_archive()

    word_file_hdr_ftr_rels: WordFileHdrFtrRels = WordFileHdrFtrRels(core_zip_file)
    word_file_hdr_ftr_rels.set_word_files()
    word_file_hdr_ftr_rels.delete_from_archive()

    word_file_rels: WordFileRelsCollection = WordFileRelsCollection(core_zip_file)
    word_file_rels.set_word_files()
    word_file_rels.add_to_archive()

    word_file_image: WordFileImage = WordFileImage(core_zip_file)

    word_file_basic: WordFileBasic = WordFileBasic(core_zip_file, (word_file_image,))
    word_file_basic.set_word_files()
    word_file_basic.add_to_archive()


def _xml_files_processing(core_zip_file: CoreZipFile, is_two_sided: bool = True):
    xml_settings: XmlSettings = XmlSettings(core_zip_file, is_two_sided)
    xml_settings.set_settings()

    xml_styles: XmlStyles = XmlStyles(core_zip_file)
    xml_styles.add_basic_styles()


def _xml_relationships_file(core_zip_file: CoreZipFile) -> XmlRelationships:
    xml_relationships: XmlRelationships = XmlRelationships(core_zip_file)
    xml_relationships.read()
    xml_relationships.set_xml_relationships()
    xml_relationships.delete_rels(xml_relationships.hdr_ftr_references().values())
    xml_relationships.save()
    return xml_relationships


def _hdr_ftr_rel_references(
        xml_relationships: XmlRelationships,
        document_mode: DocumentMode,
        is_defense_ministery: bool):
    hdr_ftr: HdrFtrRelReferenceController = HdrFtrRelReferenceController(is_defense_ministery=is_defense_ministery)
    hdr_ftr_mode: HdrFtrRelRefControllerMode = hdr_ftr.make_controller(document_mode)
    hdr_ftr_mode.generate_all()
    for k, v in hdr_ftr_mode.rel_target_rel_type.items():
        rel_id: str = f"{xml_relationships.next_rel_id()}"
        xml_relationship: XmlRelationship = XmlRelationship(rel_id, v, k, xml_relationships)
        xml_relationships.add_xml_relationship(xml_relationship)
    xml_relationships.save()
    return hdr_ftr_mode


def _xml_document_file(
        core_zip_file: CoreZipFile,
        xml_relationships: XmlRelationships,
        hdr_ftr: HdrFtrRelReferenceController):
    xml_document: XmlDocument = XmlDocument(core_zip_file)
    xml_document.read()

    for section_index in range(len(xml_document)):
        xml_section: XmlSection = XmlSection(xml_document, section_index)
        xml_section_mode: Union[XmlSectionArch, XmlSectionTypo] = xml_section.make_section()
        xml_section_mode.read()
        xml_section_mode.set_section()
        if xml_section_mode.orientation == SectionOrientation.LANDSCAPE:
            _section_index: int = -1
        elif section_index >= 2:
            _section_index: int = 2
        else:
            _section_index: int = section_index
        for header_footer in hdr_ftr.section_hdr_ftr(_section_index):
            rel_id: str = xml_relationships.hdr_ftr_references().get(header_footer.rel_target.value)
            hdr_ftr_reference: HdrFtrReference = header_footer.hdr_ftr_reference(rel_id)
            xml_section_mode.add_header_footer_reference(hdr_ftr_reference)
            xml_section_mode.write()
    # close the file, pack the archive to the docx file
    xml_document.save()
    # core_zip_file.delete_temp_archive()


def log_post_processing():
    """Deletes the logs files and folder."""
    _log_folder: Path = Path(log_folder)
    logger.stop()
    # for file in _log_folder.iterdir():
    #     file.unlink()
    # _log_folder.rmdir()
    sleep(1)
    return


def file_modify(user_input_values: UserInputValues):
    # initiate the core files and classes, unpack the docx document as the ZIP archive
    for document_mode, path in product(user_input_values.document_modes, user_input_values.path_files):
        core_zip_file: CoreZipFile = _core_preprocessing(path, document_mode)
        with core_zip_file as core_zf:
            # do operations with the xml files without parsing them
            _word_files_processing(core_zf)
            # do some changes in the xml files based on the predefined ones
            _xml_files_processing(core_zf, user_input_values.is_two_sided)
            # do some changes in the document.xml.rels file
            xml_relationships: XmlRelationships = _xml_relationships_file(core_zf)
            # do operations with the header*.xml and footer*.xml files
            hdr_ftr_rel_refs: HdrFtrRelReferenceController = _hdr_ftr_rel_references(
                xml_relationships, document_mode, user_input_values.is_defense_ministery)
            # do operations with the sectPr and headerReference/footerReference elements
            _xml_document_file(core_zf, xml_relationships, hdr_ftr_rel_refs)
        logger.info('Обработка файла "{file}" завершена.\n'.format(file=path))
    return


@logger.catch
@configure_custom_logging("docx_modify")
def run_script():
    """Main entrance point of the program."""
    show_prompt()
    document_modes: list[DocumentMode] = [DocumentMode.TYPO]
    path_files: list[Path] = [
        # Path(r"C:\Users\tarasov-a\Desktop\test\test_arch_hdr_ftr.docx"),
        Path(r"C:\Users\tarasov-a\Desktop\test\test_no_hdr_ftr.docx"),
        # Path(r"C:\Users\tarasov-a\Desktop\test\test_typo_hdr_ftr.docx"),
        # Path(r"C:\Users\tarasov-a\Desktop\test\test_with_landscape.docx")
    ]
    is_two_sided: bool = True
    is_defense_ministery: bool = True
    user_input_values: UserInputValues = UserInputValues(document_modes, path_files, is_two_sided, is_defense_ministery)
    logger.error(f"user_input_values = {user_input_values}")
    # user_input_values: Optional[UserInputValues] = get_user_input()
    if user_input_values is not None:
        file_modify(user_input_values)
    log_post_processing()
    input("Нажмите <Enter>, чтобы закрыть окно ...")
