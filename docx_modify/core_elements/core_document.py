from __future__ import annotations

from functools import cached_property
from pathlib import Path
from shutil import copy2
from typing import Union

from loguru import logger

from docx_modify.core_elements.clark_name import register_ns
from docx_modify.core_elements.updated_zip_file import FileNotInArchiveError, PathLike, UpdatedZipFile
from docx_modify.enum_element import DocumentMode
from docx_modify.main import _parent_path


class CoreDocument:
    def __init__(self, path: PathLike):
        if isinstance(path, str):
            path: Path = Path(path).resolve()
        self._path: Path = path
        self._path_dir: Path = Path(_parent_path).joinpath("_temp/")

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._path}, {self.document_mode}"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._path == other._path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._path != other._path
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self._path)

    @property
    def core_document(self):
        return self

    @property
    def document_mode(self):
        raise NotImplementedError

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @property
    def new_name(self) -> Path:
        raise NotImplementedError

    def duplicate(self):
        new_path: Path = copy2(self._path, self.new_name)
        self._path = new_path
        logger.debug(f"The file is saved to {new_path}")
        return

    def make_document(self, document_mode: DocumentMode) -> CoreDocumentMode:
        if document_mode == DocumentMode.ARCHIVE:
            return CoreDocumentArchive(self._path)
        elif document_mode == DocumentMode.TYPO:
            return CoreDocumentTypo(self._path)

    @property
    def path_dir(self):
        return self._path_dir


class CoreDocumentArchive(CoreDocument):
    def __init__(self, path: PathLike):
        super().__init__(path)
        register_ns()

    @property
    def document_mode(self) -> DocumentMode:
        return DocumentMode.ARCHIVE

    @property
    def new_name(self) -> Path:
        return self._path.with_stem(f"{self._path.stem}_арх")


class CoreDocumentTypo(CoreDocument):
    def __init__(self, path: PathLike):
        super().__init__(path)
        register_ns()

    @property
    def document_mode(self) -> DocumentMode:
        return DocumentMode.TYPO

    @property
    def new_name(self) -> Path:
        return self._path.with_stem(f"{self._path.stem}_тпг")


CoreDocumentMode = Union[CoreDocumentArchive, CoreDocumentTypo]


class CoreZipFile(CoreDocument):
    @property
    def document_mode(self) -> DocumentMode:
        return self._document_mode

    @property
    def new_name(self) -> Path:
        return self.new_name

    def __init__(self, path: PathLike, document_mode: DocumentMode):
        super().__init__(path)
        self._document_mode: DocumentMode = document_mode
        self.__updated_zip_file: UpdatedZipFile = UpdatedZipFile(self._path, self._path_dir)
        self._is_zipped: bool = True

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._path})>, {repr(self.__updated_zip_file)}, {self._is_zipped}"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._path}, {str(self.__updated_zip_file)}"

    def copy(self, name_from: PathLike, name_to: PathLike):
        return self.__updated_zip_file.copy_file(name_from, name_to)

    @property
    def files(self) -> list[str]:
        return self.__updated_zip_file.files

    def unarchive(self):
        return self.__updated_zip_file.unarchive()

    def delete(self, name: PathLike):
        return self.__updated_zip_file.delete_file(name)

    def delete_temp_archive(self):
        return self.__updated_zip_file.delete_temp_archive()

    def __getitem__(self, item):
        if item in self:
            _index: int = self.files.index(item)
            return self.files.__getitem__(_index)
        elif item in self.unzipped_files:
            return self.unzipped_files.get(item)
        else:
            logger.error(f"Файл {item} не найден в разархивированном ZIP-пакете")
            raise FileNotInArchiveError

    def __iter__(self):
        return iter(self.files)

    def __contains__(self, item):
        return item in self.files

    def __len__(self):
        return len(self.files)

    def __bool__(self):
        return len(self) > 0

    @property
    def unzipped_files(self) -> dict[str, UnzippedFile]:
        return {name: UnzippedFile(name, self) for name in self.files}

    def __enter__(self):
        self.__updated_zip_file = self.__updated_zip_file.__enter__()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__updated_zip_file.__exit__(exc_type, exc_val, exc_tb)
        self.__updated_zip_file.close()


class UnzippedFile:
    def __init__(self, name: str, core_zip_file: CoreZipFile):
        self._name: str = name
        self._core_zip_file: CoreZipFile = core_zip_file
        self._path_dir: Path = core_zip_file.path_dir

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._name}, {repr(self._core_zip_file)})>," \
               f" {self._core_zip_file.path}"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._name}, {str(self._core_zip_file)}, {self._core_zip_file.path}"

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @cached_property
    def path(self) -> Path:
        return self._core_zip_file.path

    @property
    def full_path(self) -> Path:
        """The full path to the file in the unpacked archive."""
        return self._core_zip_file.path_dir.joinpath(self.name)

    def __hash__(self):
        return hash(self._name)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._name == other._name
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._name != other._name
        else:
            return NotImplemented
