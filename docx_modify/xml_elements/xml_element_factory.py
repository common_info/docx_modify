from typing import Any, Iterable, Mapping, Optional

from lxml.etree import Element, ElementBase

from docx_modify.core_elements.clark_name import fqdn


class XmlElementFactory:
    @classmethod
    def __call__(cls, *args, **kwargs):
        return cls()

    @staticmethod
    def generate_xml_element(
            tag: str, *,
            children: Optional[Iterable[ElementBase]] = None,
            attributes: Mapping[str, Any] = None) -> ElementBase:
        if children is None:
            children: list[ElementBase] = []
        if attributes is None:
            attributes: dict[str, Any] = dict()
        _attrs: dict[str, Any] = {fqdn(k): f"{v}" for k, v in attributes.items()}
        element: ElementBase = Element(fqdn(tag), _attrs)
        element.extend(children)
        return element


new_xml = XmlElementFactory.generate_xml_element
