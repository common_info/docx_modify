from pathlib import Path
from typing import Optional

from loguru import logger
from lxml import etree
# noinspection PyProtectedMember
from lxml.etree import ElementBase, _ElementTree

from docx_modify.core_elements.clark_name import fqdn
from docx_modify.core_elements.core_document import CoreZipFile, parent_path
from docx_modify.xml_elements.xml_file import XmlFile


class XmlStyles(XmlFile):

    _styles_add: tuple[str, ...] = (
        "_style11", "_style110", "_style12_header", "_style12_footer", "_style14", "_style16", "_table_grid")

    def __init__(self, core_zip_file: CoreZipFile):
        name: str = "word/styles.xml"
        super().__init__(name, core_zip_file)
        self._basic_file: Path = parent_path.joinpath(f"sources/{self._core_zip_file.mode.value}/styles/styles.xml")

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.path})>"

    def __iter__(self):
        return iter(self.styles)

    def __contains__(self, item):
        return item in self.styles

    def __hash__(self):
        return hash(self._name)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    @property
    def styles(self) -> list[ElementBase]:
        return self._content.getchildren()

    def __getitem__(self, item):
        return self.styles.__getitem__(item)

    def __setitem__(self, key, value):
        self.styles.__setitem__(key, value)

    def basic_styles(self) -> list[ElementBase]:
        _et: _ElementTree = etree.parse(self._basic_file)
        _root: ElementBase = _et.getroot()
        return _root.getchildren()

    @property
    def _styles_id(self) -> list[str]:
        return [child.get(fqdn("w:styleId")) for child in self.styles]

    def add_basic_styles(self):
        for style in self._styles_add:
            if style in self._styles_id:
                _style_xml: Optional[ElementBase] = self.find_child_element("w:styleId", style)
                self.delete_child_element(_style_xml)
        self.read()
        self._content.extend(self.basic_styles())
        self.write()
        logger.debug("XML styles have been added")
