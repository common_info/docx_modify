from pathlib import Path

from loguru import logger
from lxml.etree import ElementBase

from docx_modify.core_elements.core_document import CoreZipFile
from docx_modify.xml_elements.xml_file import XmlFile


class XmlDocument(XmlFile):
    @property
    def full_path(self) -> Path:
        return super().full_path

    def __init__(self, core_zip_file: CoreZipFile):
        self._name: str = "word/document.xml"
        super().__init__(self._name, core_zip_file)

    def __repr__(self):
        return f"<{self.__class__.__name__}({repr(self._core_zip_file)})>"

    def __str__(self):
        return f"{self.__class__.__name__}: {self._name}"

    def __len__(self):
        return len(self.sect_prs())

    def sect_prs(self) -> list[ElementBase]:
        return self.get_children_elements("w:sectPr")

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        raise AttributeError

    def _path_to_save(self) -> Path:
        return self._core_zip_file.path_dir.joinpath(self._name)

    def save(self):
        self.content_tree.write(self._path_to_save(), encoding="utf-8", xml_declaration=True, standalone=True)
        self.read()
        logger.debug(f"Document is saved to {self._path_to_save()}")
