from typing import Optional

from loguru import logger
from lxml.etree import ElementBase

from docx_modify.core_elements.clark_name import fqdn
from docx_modify.core_elements.core_document import CoreZipFile
from docx_modify.xml_elements.xml_element_factory import new_xml
from docx_modify.xml_elements.xml_file import XmlFile


class XmlSettings(XmlFile):
    def __init__(self, updated_zip_file: CoreZipFile, is_mirror: bool = True):
        name: str = "word/settings.xml"
        super().__init__(name, updated_zip_file)
        self._is_mirror: bool = is_mirror

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.path})>"

    def __iter__(self):
        return iter(self.settings)

    def __contains__(self, item):
        return item in self.settings

    def __hash__(self):
        return hash(self._name)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return True
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return False
        else:
            return NotImplemented

    @property
    def settings(self) -> list[ElementBase]:
        return self._content.getchildren()

    def __getitem__(self, item):
        return self.settings.__getitem__(item)

    def __setitem__(self, key, value):
        self.settings.__setitem__(key, value)

    def _set_mirror_margins(self):
        mirror_margins: Optional[ElementBase] = self.get_child_element("w:mirrorMargins")
        if mirror_margins is None:
            mirror_margins: ElementBase = new_xml("w:mirrorMargins")
            self.add_child_element(mirror_margins)
        if self._is_mirror:
            mirror_margins.set(fqdn("w:val"), "true")
            logger.debug("Mirror margins are implemented")
        else:
            mirror_margins.set(fqdn("w:val"), "false")
            logger.debug("Mirror margins are deleted")
        return

    def _set_parameter(self, child: str, tag: str, value: str):
        _param: Optional[ElementBase] = self.get_child_element(child)
        if _param is None:
            _param: ElementBase = new_xml(child)
            self.add_child_element(_param)
        _param.set(fqdn(tag), value)
        return

    def set_settings(self):
        self.read()
        self._set_mirror_margins()
        self._set_parameter("w:evenAndOddHeaders", "w:val", "1")
        logger.debug("Even and odd headers and footers are implemented")
        self._set_parameter("w:bordersDoNotSurroundHeader", "w:val", "0")
        logger.debug("Headers are surrounded by the page borders")
        self._set_parameter("w:bordersDoNotSurroundFooter", "w:val", "0")
        logger.debug("Footers are surrounded by the page borders")
        self.write()
