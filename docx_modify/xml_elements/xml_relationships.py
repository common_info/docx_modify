from typing import Iterable, Union

from loguru import logger
from lxml.etree import ElementBase

from docx_modify.core_elements.core_document import CoreZipFile
from docx_modify.enum_element import XmlRelationshipTarget, XmlRelationshipType
from docx_modify.xml_elements.xml_element_factory import new_xml
from docx_modify.xml_elements.xml_file import XmlFile


class XmlRelationships(XmlFile):
    def __init__(self, core_zip_file: CoreZipFile):
        name: str = "word/_rels/document.xml.rels"
        super().__init__(name, core_zip_file)
        self._xml_relationships: dict[str, XmlRelationship] = dict()

    def __str__(self):
        return f"{self.__class__.__name__}: {self.name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._core_zip_file})>"

    def __getitem__(self, item):
        return self._xml_relationships.__getitem__(item)

    def __iter__(self):
        return iter(self._xml_relationships.values())

    def __contains__(self, item):
        if isinstance(item, str):
            return item in self._xml_relationships.keys()
        elif isinstance(item, int):
            return f"rId{item}" in self._xml_relationships.keys()
        elif isinstance(item, XmlRelationship):
            return item in self._xml_relationships.values()
        else:
            return False

    def set_xml_relationships(self):
        for child in self.children():
            rel_id: str = child.get("Id")
            rel_type: XmlRelationshipType = XmlRelationshipType(child.get("Type"))
            rel_target: str = child.get("Target")
            self._xml_relationships[rel_id] = XmlRelationship(rel_id, rel_type, rel_target, self)

    def __add__(self, other):
        if isinstance(other, XmlRelationship):
            xml_relationship: XmlRelationship = other
        elif isinstance(other, tuple) and len(other) == 2 and \
                isinstance(other[0], XmlRelationshipType) and isinstance(other[1], str):
            rel_type, rel_target = other
            xml_relationship: XmlRelationship = self.generate_xml_relationship(rel_type, rel_target)
        else:
            return NotImplemented
        self._xml_relationships[xml_relationship.rel_id] = xml_relationship
        rel: ElementBase = new_xml("Relationship", attributes=xml_relationship.attrs)
        self._content.append(rel)
        self.write()
        logger.debug(f"Relationship {xml_relationship.rel_id}, Target {xml_relationship.rel_target} is added")
        return

    __radd__ = __add__
    __iadd__ = __add__

    def __delitem__(self, key):
        element: ElementBase = self.find_child_element("Id", key)
        if element is not None:
            self._content.remove(element)
            self._xml_relationships.__delitem__(key)
        return

    @property
    def _rel_ids(self) -> list[int]:
        return list(map(lambda x: int(f"{x[3:]}"), self._xml_relationships.keys()))

    def next_rel_id(self) -> int:
        return max(self._rel_ids) + 1

    def generate_xml_relationship(
            self,
            rel_type: XmlRelationshipType,
            rel_target: XmlRelationshipTarget) -> 'XmlRelationship':
        rel_id: str = f"rId{self.next_rel_id()}"
        return XmlRelationship(rel_id, rel_type, rel_target, self)

    def delete_rels(self, rels: Iterable[Union[str, int, 'XmlRelationship']] = None):
        if rels is None:
            rels: list[str] = [*self._xml_relationships.keys()]
        for xml_relationship in rels:
            if isinstance(xml_relationship, str):
                self.__delitem__(xml_relationship)
            elif isinstance(xml_relationship, int):
                self.__delitem__(f"rId{xml_relationship}")
            elif isinstance(xml_relationship, XmlRelationship):
                self.__delitem__(xml_relationship.rel_id)
        self.write()
        return

    def add_xml_relationship(self, xml_relationship: 'XmlRelationship'):
        _attrs: dict[str, str] = {
            "Id": f"rId{xml_relationship.rel_id}",
            "Type": xml_relationship.rel_type.value,
            "Target": xml_relationship.rel_target
        }
        element: ElementBase = new_xml("Relationship", attributes=_attrs)
        self.add_child_element(element)
        self._xml_relationships[f"rId{xml_relationship.rel_id}"] = xml_relationship
        logger.debug(f"Relationship {xml_relationship.rel_id} is added to the document")
        self.write()
        self.read()

    def hdr_ftr_references(self):
        _hdr_ftr_references: dict[str, str] = dict()
        for k, v in self._xml_relationships.items():
            if v.rel_type in (XmlRelationshipType.HEADER, XmlRelationshipType.FOOTER):
                _hdr_ftr_references[v.rel_target] = k
        return _hdr_ftr_references


class XmlRelationship:
    def __init__(
            self,
            rel_id: str,
            rel_type: XmlRelationshipType,
            rel_target: Union[XmlRelationshipTarget, str],
            xml_relationships: XmlRelationships):
        if isinstance(rel_target, XmlRelationshipTarget):
            rel_target: str = rel_target.value
        self._rel_id: str = rel_id
        self._rel_type: XmlRelationshipType = rel_type
        self._rel_target: str = rel_target
        self._parent: XmlRelationships = xml_relationships

    def __str__(self):
        return f"{self.__class__.__name__}: {self.rel_id}, {str(self.rel_type)}, {self.rel_target}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.rel_id}, {repr(self.rel_type)}, {self.rel_target})>"

    def __hash__(self):
        return hash(self._rel_id)

    @property
    def rid(self) -> int:
        return int(self._rel_id[3:])

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.rid == other.rid
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.rid != other.rid
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__):
            return self.rid > other.rid
        else:
            return NotImplemented

    def __ge__(self, other):
        if isinstance(other, self.__class__):
            return self.rid >= other.rid
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__):
            return self.rid < other.rid
        else:
            return NotImplemented

    def __le__(self, other):
        if isinstance(other, self.__class__):
            return self.rid <= other.rid
        else:
            return NotImplemented

    def __bool__(self):
        return True

    @property
    def rel_id(self):
        return self._rel_id

    @rel_id.setter
    def rel_id(self, value):
        self._rel_id = value

    @property
    def rel_type(self):
        return self._rel_type

    @rel_type.setter
    def rel_type(self, value):
        self._rel_type = value

    @property
    def rel_target(self):
        return self._rel_target

    @rel_target.setter
    def rel_target(self, value):
        self._rel_target = value

    @property
    def attrs(self):
        return {
            "Id": self.rel_id,
            "Type": self.rel_type,
            "Target": self.rel_target
        }
