from __future__ import annotations

from typing import Iterable, NamedTuple, TypeAlias

from loguru import logger

from docx_modify.enum_element import DocumentMode, XmlHdrFtrReference, XmlReference, XmlRelationshipTarget, \
    XmlRelationshipType


class HdrFtrReference(NamedTuple):
    reference: XmlReference
    rid: str
    ref_type: XmlHdrFtrReference

    def __str__(self):
        return f"{self.__class__.__name__}: {self.reference.value}, {self.rid}, {self.ref_type}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.reference.value}, {self.rid}, {self.ref_type})>"


class _HdrFtrRelationshipReference(NamedTuple):
    name: str
    section_index: int
    rel_target: XmlRelationshipTarget
    reference: XmlReference
    reference_type: XmlHdrFtrReference
    rel_type: XmlRelationshipType

    def __str__(self):
        return f"{self.__class__.__name__}: {self._asdict().values()}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict().items()})>"

    def hdr_ftr_reference(self, rel_id: str) -> HdrFtrReference:
        return HdrFtrReference(self.reference, rel_id, self.reference_type)


class HdrFtrRelReferenceController(dict):
    def __init__(
            self,
            hdr_ftr_items: Iterable[_HdrFtrRelationshipReference] = None, *,
            is_defense_ministery: bool = False):
        super().__init__()
        self._is_defense_ministery: bool = is_defense_ministery
        if hdr_ftr_items is not None:
            for hdr_ftr in hdr_ftr_items:
                self.__setitem__(hdr_ftr.name, hdr_ftr)

    def __call__(self, *args, **kwargs):
        return self.__class__()

    @property
    def rel_target_rel_type(self) -> dict[XmlRelationshipTarget, XmlRelationshipType]:
        return {rel_ref.rel_target: rel_ref.rel_type for rel_ref in self.values()}

    def _generate_hdr_ftr(
            self,
            name: str,
            section_index: int,
            rel_target: XmlRelationshipTarget,
            reference: XmlReference,
            reference_type: XmlHdrFtrReference,
            rel_type: XmlRelationshipType):
        hdr_ftr: _HdrFtrRelationshipReference
        hdr_ftr = _HdrFtrRelationshipReference(
            name, section_index, rel_target, reference, reference_type, rel_type)
        self.__setitem__(name, hdr_ftr)

    def values(self) -> list[_HdrFtrRelationshipReference]:
        return [v for v in super().values()]

    def keys(self) -> list[str]:
        return [k for k in super().keys()]

    def items(self) -> list[tuple[str, _HdrFtrRelationshipReference]]:
        return [(k, v) for k, v in super().items()]

    def section_hdr_ftr(self, section_index: int) -> list[_HdrFtrRelationshipReference]:
        return list(filter(lambda x: x.section_index == section_index, self.values()))

    def make_controller(self, document_mode: DocumentMode) -> HdrFtrRelRefControllerMode:
        if document_mode == DocumentMode.ARCHIVE:
            return HdrFtrRelRefControllerArch(*self.items())
        elif document_mode == DocumentMode.TYPO:
            return HdrFtrRelRefControllerTypo(*self.items())


class HdrFtrRelRefControllerTypo(HdrFtrRelReferenceController):
    def _zero_first_footer(self):
        return self._generate_hdr_ftr(
            "zero_first_footer", 0,
            XmlRelationshipTarget.TYPO_ZERO_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.FIRST,
            XmlRelationshipType.FOOTER)

    def _zero_even_footer(self):
        return self._generate_hdr_ftr(
            "zero_even_footer", 0,
            XmlRelationshipTarget.TYPO_DEFAULT_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.EVEN,
            XmlRelationshipType.FOOTER)

    def _zero_default_footer(self):
        return self._generate_hdr_ftr(
            "zero_default_footer", 0,
            XmlRelationshipTarget.TYPO_ODD_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.FOOTER)

    def _zero_first_header(self):
        return self._generate_hdr_ftr(
            "zero_first_header", 0,
            XmlRelationshipTarget.TYPO_DEFAULT_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.FIRST,
            XmlRelationshipType.HEADER)

    def _zero_even_header(self):
        return self._generate_hdr_ftr(
            "zero_even_header", 0,
            XmlRelationshipTarget.TYPO_DEFAULT_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.EVEN,
            XmlRelationshipType.HEADER)

    def _zero_default_header(self):
        return self._generate_hdr_ftr(
            "zero_default_header", 0,
            XmlRelationshipTarget.TYPO_DEFAULT_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.HEADER)

    def _one_first_header(self):
        return self._generate_hdr_ftr(
            "one_first_header", 1,
            XmlRelationshipTarget.TYPO_ONE_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.FIRST,
            XmlRelationshipType.HEADER)

    def _one_first_footer(self):
        if not self._is_defense_ministery:
            return self._generate_hdr_ftr(
                "one_first_footer", 1,
                XmlRelationshipTarget.TYPO_ONE_FOOTER_XML,
                XmlReference.FOOTER,
                XmlHdrFtrReference.FIRST,
                XmlRelationshipType.FOOTER)
        else:
            logger.info(f"xml file defense ministery {XmlRelationshipTarget.TYPO_ONE_FOOTER_DEFENSE_XML}")
            return self._generate_hdr_ftr(
                "one_first_footer", 1,
                XmlRelationshipTarget.TYPO_ONE_FOOTER_DEFENSE_XML,
                XmlReference.FOOTER,
                XmlHdrFtrReference.FIRST,
                XmlRelationshipType.FOOTER)

    def _one_even_footer(self):
        return self._generate_hdr_ftr(
            "one_even_footer", 1,
            XmlRelationshipTarget.TYPO_EVEN_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.EVEN,
            XmlRelationshipType.FOOTER)

    def _one_even_header(self):
        return self._generate_hdr_ftr(
            "one_even_header", 1,
            XmlRelationshipTarget.TYPO_DEFAULT_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.EVEN,
            XmlRelationshipType.HEADER)

    def _one_odd_header(self):
        return self._generate_hdr_ftr(
            "one_default_header", 1,
            XmlRelationshipTarget.TYPO_DEFAULT_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.HEADER)

    def _one_odd_footer(self):
        return self._generate_hdr_ftr(
            "one_odd_footer", 1,
            XmlRelationshipTarget.TYPO_ODD_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.FOOTER)

    def _two_even_footer(self):
        return self._generate_hdr_ftr(
            "two_even_footer", 2,
            XmlRelationshipTarget.TYPO_EVEN_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.EVEN,
            XmlRelationshipType.FOOTER)

    def _two_odd_footer(self):
        return self._generate_hdr_ftr(
            "two_odd_footer", 2,
            XmlRelationshipTarget.TYPO_ODD_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.FOOTER)

    def _two_even_header(self):
        return self._generate_hdr_ftr(
            "two_even_header", 2,
            XmlRelationshipTarget.TYPO_DEFAULT_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.EVEN,
            XmlRelationshipType.HEADER)

    def _two_odd_header(self):
        return self._generate_hdr_ftr(
            "two_odd_header", 2,
            XmlRelationshipTarget.TYPO_DEFAULT_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.HEADER)

    def _footer_landscape_even(self):
        return self._generate_hdr_ftr(
            "footer_landscape_even", -1,
            XmlRelationshipTarget.TYPO_FOOTER_LANDSCAPE_EVEN_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.EVEN,
            XmlRelationshipType.FOOTER)

    def _footer_landscape_odd(self):
        return self._generate_hdr_ftr(
            "footer_landscape_odd", -1,
            XmlRelationshipTarget.TYPO_FOOTER_LANDSCAPE_ODD_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.FOOTER)

    def _header_landscape_even(self):
        return self._generate_hdr_ftr(
            "header_landscape_even", -1,
            XmlRelationshipTarget.TYPO_HEADER_LANDSCAPE_EVEN_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.EVEN,
            XmlRelationshipType.HEADER)

    def _header_landscape_odd(self):
        return self._generate_hdr_ftr(
            "header_landscape_odd", -1,
            XmlRelationshipTarget.TYPO_HEADER_LANDSCAPE_ODD_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.HEADER)

    def generate_all(self):
        self._zero_first_footer()
        self._zero_even_footer()
        self._zero_default_footer()

        self._zero_first_header()
        self._zero_even_header()
        self._zero_default_header()

        self._one_first_header()
        self._one_first_footer()
        self._one_even_footer()
        self._one_odd_footer()

        self._two_even_footer()
        self._two_odd_footer()
        self._two_even_header()
        self._two_odd_header()

        self._footer_landscape_even()
        self._footer_landscape_odd()
        self._header_landscape_even()
        self._header_landscape_odd()


class HdrFtrRelRefControllerArch(HdrFtrRelReferenceController):
    def _zero_even_footer(self):
        return self._generate_hdr_ftr(
            "zero_even_footer", 0,
            XmlRelationshipTarget.ARCH_EVEN_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.EVEN,
            XmlRelationshipType.FOOTER)

    def _zero_default_footer(self):
        return self._generate_hdr_ftr(
            "zero_default_footer", 0,
            XmlRelationshipTarget.ARCH_FIRST_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.FOOTER)

    def _zero_even_header(self):
        return self._generate_hdr_ftr(
            "zero_even_header", 0,
            XmlRelationshipTarget.ARCH_EVEN_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.EVEN,
            XmlRelationshipType.HEADER)

    def _zero_default_header(self):
        return self._generate_hdr_ftr(
            "zero_default_header", 0,
            XmlRelationshipTarget.ARCH_FIRST_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.HEADER)

    def _one_first_header(self):
        return self._generate_hdr_ftr(
            "one_first_header", 1,
            XmlRelationshipTarget.ARCH_DEFAULT_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.FIRST,
            XmlRelationshipType.HEADER)

    def _one_first_footer(self):
        return self._generate_hdr_ftr(
            "one_first_footer", 1,
            XmlRelationshipTarget.ARCH_EVEN_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.FIRST,
            XmlRelationshipType.FOOTER)

    def _one_default_footer(self):
        return self._generate_hdr_ftr(
            "one_default_footer", 1,
            XmlRelationshipTarget.ARCH_DEFAULT_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.FOOTER)

    def _one_default_header(self):
        return self._generate_hdr_ftr(
            "one_default_header", 1,
            XmlRelationshipTarget.ARCH_EVEN_HEADER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.FOOTER)

    def _two_even_footer(self):
        return self._generate_hdr_ftr(
            "two_even_footer", 2,
            XmlRelationshipTarget.ARCH_DEFAULT_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.EVEN,
            XmlRelationshipType.FOOTER)

    def _two_odd_footer(self):
        return self._generate_hdr_ftr(
            "two_odd_footer", 2,
            XmlRelationshipTarget.ARCH_EVEN_FOOTER_XML,
            XmlReference.FOOTER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.FOOTER)

    def _two_even_header(self):
        return self._generate_hdr_ftr(
            "two_even_header", 2,
            XmlRelationshipTarget.ARCH_EVEN_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.EVEN,
            XmlRelationshipType.HEADER)

    def _two_odd_header(self):
        return self._generate_hdr_ftr(
            "two_odd_header", 2,
            XmlRelationshipTarget.ARCH_DEFAULT_HEADER_XML,
            XmlReference.HEADER,
            XmlHdrFtrReference.DEFAULT,
            XmlRelationshipType.HEADER)

    def generate_all(self):
        self._zero_even_footer()
        self._zero_default_footer()
        self._zero_even_header()
        self._zero_default_header()

        self._one_first_header()
        self._one_default_header()
        self._one_first_footer()
        self._one_default_footer()

        self._two_even_footer()
        self._two_odd_footer()
        self._two_even_header()
        self._two_odd_header()


HdrFtrRelRefControllerMode: TypeAlias = HdrFtrRelRefControllerArch | HdrFtrRelRefControllerTypo
