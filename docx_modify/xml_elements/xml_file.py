from typing import Generator, Optional, Union

from loguru import logger
from lxml import etree
# noinspection PyProtectedMember
from lxml.etree import ElementBase, _ElementTree

from docx_modify.core_elements.clark_name import fqdn
from docx_modify.core_elements.core_document import CoreZipFile, UnzippedFile
from docx_modify.enum_element import DocumentMode


class XmlFile(UnzippedFile):
    def __init__(self, name: str, core_zip_file: CoreZipFile):
        super().__init__(name, core_zip_file)
        self._etree: Optional[_ElementTree] = None
        self._content: Optional[ElementBase] = None
        self.read()

    def __str__(self):
        return f"{self.__class__.__name__}: {self._name}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({repr(self._core_zip_file)})>"

    def __hash__(self):
        return hash(self._name)

    @property
    def content_tree(self) -> _ElementTree:
        return self._content.getroottree()

    @property
    def mode(self) -> DocumentMode:
        return self._core_zip_file.mode

    def write(self):
        self.content_tree.write(self.full_path, encoding="utf-8", xml_declaration=True, standalone=True)
        logger.debug(f"The content is written to the file {self.full_path}")
        self.read()

    def read(self):
        self._etree = etree.parse(self.full_path)
        self._content = self._etree.getroot()

    def iter_child_elements(self, tag: str) -> Generator[ElementBase, None, None]:
        element: ElementBase
        for element in self._content.iterdescendants(fqdn(tag)):
            yield element

    def get_child_element(self, tag: str) -> Optional[ElementBase]:
        return self._content.find(fqdn(tag))

    def get_children_elements(self, tag: str) -> list[ElementBase]:
        return list(self.iter_child_elements(tag))

    def add_child_element(self, child: ElementBase):
        return self._content.append(child)

    def delete_child_element(self, child: ElementBase):
        return self._content.remove(child)

    def children(self) -> list[ElementBase]:
        return self._content.getchildren()

    def find_child_element(self, attr: str, value: str) -> Optional[ElementBase]:
        for element in self.children():
            if element.get(fqdn(attr)) == value:
                return element
        else:
            return None

    def save(self):
        if self.content_tree != self._etree:
            self.write()
            self.read()
        return


class XmlFilePart:
    def __init__(self, tag: str, xml_file: XmlFile, idx: Optional[int] = None):
        if idx is None:
            idx: int = -1
        self._xml_file: XmlFile = xml_file
        self._tag: str = tag
        self._idx: int = idx
        self._content: Optional[ElementBase] = None
        self._prev_state: Optional[ElementBase] = None

    def parent(self):
        return self._prev_state.getparent()

    @property
    def mode(self) -> DocumentMode:
        return self._xml_file.mode

    def read(self):
        if self._idx == -1:
            self._prev_state = self._xml_file.get_child_element(self._tag)
        else:
            self._prev_state = self._xml_file.get_children_elements(self._tag).__getitem__(self._idx)
        self._content = self._prev_state

    def write(self):
        self.parent().replace(self._prev_state, self._content)
        self.read()

    def get_child_element(self, tag: str) -> ElementBase:
        return self._content.find(fqdn(tag))

    def get_children_elements(self, tag: Optional[str] = None) -> list[ElementBase]:
        if tag is None:
            return self._content.getchildren()
        else:
            return self._content.findall(fqdn(tag))

    def add_child_element(self, child: ElementBase):
        return self._content.append(child)

    def insert_child_element(self, child: ElementBase, pos: int = -1):
        if pos == -1:
            return self.add_child_element(child)
        return self._content.insert(pos, child)

    def delete_child_element(self, child: Union[str, ElementBase]):
        if isinstance(child, str):
            _child: ElementBase = self.get_child_element(child)
            if _child is None:
                logger.debug(f"The child {child} is not found")
                return
        else:
            _child = child
        return self._content.remove(_child)
