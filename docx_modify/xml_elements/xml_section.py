from typing import Optional, Union

from loguru import logger
from lxml.etree import ElementBase

from docx_modify.core_elements.clark_name import fqdn
from docx_modify.enum_element import DocumentMode, SectionOrientation, SectionPgBorder, SectionPgMar, SectionPgSz
from docx_modify.exceptions import InvalidOrientationError
from docx_modify.xml_elements.xml_document import XmlDocument
from docx_modify.xml_elements.xml_element_factory import new_xml
from docx_modify.xml_elements.xml_file import XmlFilePart
from docx_modify.xml_elements.xml_hdr_ftr import HdrFtrReference


class XmlSection(XmlFilePart):
    def __init__(self, xml_document: XmlDocument, section_index: int):
        tag: str = "w:sectPr"
        super().__init__(tag, xml_document, section_index)
        self._xml_document: XmlDocument = xml_document
        self._section_index: int = self._idx

    def make_section(self):
        if self.mode == DocumentMode.ARCHIVE:
            return XmlSectionArch(self._xml_document, self._section_index)
        elif self.mode == DocumentMode.TYPO:
            return XmlSectionTypo(self._xml_document, self._section_index)

    @property
    def _pg_sz(self) -> ElementBase:
        return self.get_child_element("w:pgSz")

    def _set_pg_num_type(self):
        if self._section_index != 1:
            return
        _pg_numbering: Optional[ElementBase] = self.get_child_element("w:pgNumType")
        if _pg_numbering is not None:
            self.delete_child_element(_pg_numbering)
        _attrs: dict[str, str] = {"w:start": "3"}
        _pg_num_type: ElementBase = new_xml("w:pgNumType", attributes=_attrs)
        self.add_child_element(_pg_num_type)
        logger.debug(f"Section {self._section_index}, page numbering starting from 3 is implemented")
        return

    def _set_pg_sz(self):
        _conversion_table: dict[SectionOrientation, SectionPgSz] = {
            SectionOrientation.PORTRAIT: SectionPgSz.a4_portrait(),
            SectionOrientation.LANDSCAPE: SectionPgSz.a4_landscape()
        }
        element: ElementBase = _conversion_table[self.orientation].element
        element.set(fqdn("w:offsetFrom"), "page")
        if self._pg_sz is not None:
            self.delete_child_element(self._pg_sz)
        self.add_child_element(element)
        logger.debug(f"Section {self._section_index}, tag <w:pgSz> is added")
        return

    @property
    def orientation(self) -> SectionOrientation:
        orient: Optional[str] = self._pg_sz.get(fqdn("w:orient"))
        if orient is None or orient == "portrait":
            return SectionOrientation.PORTRAIT
        elif orient == "landscape":
            return SectionOrientation.LANDSCAPE
        else:
            logger.error(f"Некорректная ориентация секции {orient}")
            raise InvalidOrientationError

    def _delete_title_pg(self):
        if self.orientation == SectionOrientation.LANDSCAPE:
            logger.debug(f"Section {self._section_index}, landscape, tag <w:titlePg> is deleted")
        elif self._section_index not in (0, 1):
            logger.debug(f"Section {self._section_index}, tag <w:titlePg> is deleted")
        return self.delete_child_element("w:titlePg")

    def _set_title_pg(self):
        if self._section_index in (0, 1):
            if self.get_child_element("w:titlePg") is None:
                title_pg: ElementBase = new_xml("w:titlePg")
                logger.debug(f"Section {self._section_index}, tag <w:titlePg> is added")
                return self.add_child_element(title_pg)
        else:
            title_pg: Optional[ElementBase] = self.get_child_element("w:titlePg")
            if title_pg is not None:
                logger.debug(f"Section {self._section_index}, tag <w:titlePg> is deleted")
                return self.delete_child_element(title_pg)

    def set_section(self):
        self._set_pg_sz()
        self._set_pg_borders()
        self._set_pg_mar()
        self._set_title_pg()
        self._set_pg_num_type()
        self._delete_header_footer_references()
        logger.debug(f"Section {self._section_index} is set up")

    def _get_pg_border(self, side: Union[SectionPgBorder, str]):
        raise NotImplementedError

    @property
    def _pg_border_space(self) -> dict[str, int]:
        raise NotImplementedError

    def _set_pg_border_items(self) -> list[ElementBase]:
        return [self._get_pg_border(side) for side in self._pg_border_space]

    def _set_pg_borders(self):
        self.delete_child_element("w:pgBorders")
        _attrs: dict[str, str] = {"w:offsetFrom": "text"}
        _pg_borders: ElementBase = new_xml("w:pgBorders", children=self._set_pg_border_items(), attributes=_attrs)
        self.add_child_element(_pg_borders)
        logger.debug(f"Section {self._section_index}, borders are added to the document")
        return

    def _get_header_references(self) -> list[ElementBase]:
        return self.get_children_elements("w:headerReference")

    def _get_footer_references(self) -> list[ElementBase]:
        return self.get_children_elements("w:footerReference")

    def _header_footer_references(self) -> tuple[ElementBase, ...]:
        return tuple(el for el in [*self._get_header_references(), *self._get_footer_references()])

    def _delete_header_footer_references(self):
        for element in self._header_footer_references():
            self.delete_child_element(element)

    def add_header_footer_reference(self, hdr_ftr_reference: HdrFtrReference):
        _attrs: dict[str, str] = {
            str(fqdn("r:id")): f"{hdr_ftr_reference.rid}",
            str(fqdn("w:type")): hdr_ftr_reference.ref_type.value
        }
        element: ElementBase = new_xml(hdr_ftr_reference.reference.value, attributes=_attrs)
        self.insert_child_element(element, 0)
        logger.debug(f"Section {self._section_index}, Reference {hdr_ftr_reference.rid} is added to the document")
        return

    def _set_pg_mar(self):
        raise NotImplementedError


class XmlSectionArch(XmlSection):
    _pg_mar_section: dict[int, SectionPgMar] = {
        0: SectionPgMar.archive_zero_section(),
        1: SectionPgMar.archive_one_section()
    }
    _pg_border_space_arch: dict[str, int] = {
        "top": 24,
        "bottom": 24,
        "left": 24,
        "right": 24
    }

    @property
    def _pg_border_space(self) -> dict[str, int]:
        return self._pg_border_space_arch

    def _set_pg_mar(self):
        if self._section_index in self._pg_mar_section:
            pg_mar: SectionPgMar = self._pg_mar_section.get(self._section_index)
        else:
            pg_mar: SectionPgMar = self._pg_mar_section.get(1)
        self.delete_child_element("w:pgMar")
        logger.debug(f"Section {self._section_index} {pg_mar.__class__.__name__} is implemented")
        return self.add_child_element(pg_mar.element)

    def _get_pg_border(self, side: Union[SectionPgBorder, str]) -> ElementBase:
        if isinstance(side, SectionPgBorder):
            side: str = side.value
        _attrs: dict[str, str] = {
            "w:color": "auto",
            "w:space": f"{self._pg_border_space[side]}",
            "w:sz": "0",
            "w:val": "nil"
        }
        return new_xml(f"w:{side}", attributes=_attrs)


class XmlSectionTypo(XmlSection):
    _pg_mar_section: dict[int, SectionPgMar] = {
        -1: SectionPgMar.typo_landscape_section(),
        0: SectionPgMar.typo_zero_section(),
        1: SectionPgMar.typo_one_section(),
        2: SectionPgMar.typo_two_section()
    }
    _pg_border_space_portrait: dict[str, int] = {
        "top": 31,
        "bottom": 0,
        "left": 14,
        "right": 14
    }
    _pg_border_space_landscape: dict[str, int] = {
        "top": 21,
        "bottom": 0,
        "left": 21,
        "right": 19
    }

    @property
    def _pg_border_space(self) -> dict[str, int]:
        if self.orientation == SectionOrientation.PORTRAIT:
            return self._pg_border_space_portrait
        elif self.orientation == SectionOrientation.LANDSCAPE:
            return self._pg_border_space_landscape
        else:
            logger.error(f"Некорректная ориентация секции {self.orientation}")
            raise InvalidOrientationError

    def _get_pg_border(self, side: Union[SectionPgBorder, str]):
        if isinstance(side, SectionPgBorder):
            side: str = side.value
        _attrs: dict[str, str] = {
            "w:color": "auto",
            "w:space": f"{self._pg_border_space[side]}",
            "w:sz": "12",
            "w:val": "single"
        }
        return new_xml(f"w:{side}", attributes=_attrs)

    def _set_pg_mar(self):
        if self.orientation == SectionOrientation.LANDSCAPE:
            pg_mar: SectionPgMar = self._pg_mar_section.get(-1)
        elif self._section_index not in self._pg_mar_section:
            pg_mar: SectionPgMar = self._pg_mar_section.get(2)
        else:
            pg_mar: SectionPgMar = self._pg_mar_section.get(self._section_index)
        self.delete_child_element("w:pgMar")
        logger.debug(f"Section {self._section_index} {pg_mar.__class__.__name__} is implemented")
        return self.add_child_element(pg_mar.element)
