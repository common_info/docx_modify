import base64
import faulthandler
import json
from enum import Enum
from http.client import HTTPResponse
from pathlib import Path
from sys import platform as pl
from typing import Any, Optional
from urllib.parse import quote_plus
from urllib.request import Request, urlopen

_PROTOCOL: str = "HTTPS"
_CHUNK_SIZE: int = 4096
_gitlab: dict[str, str] = {
    "win32": "docx_modify.exe",
    "darwin": "docx_modify",
    "linux": "docx_modify"
}


class RequiredAttributeMissingError(AttributeError):
    """Required attribute has no specified value."""


class UnknownPlatformError(ValueError):
    """Required attribute has no specified value."""


class CustomPort(Enum):
    SSH = 22
    FTP = 23
    HTTP = 80
    HTTPS = 443

    def __str__(self):
        return f"{self.__class__.__name__}.{self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self.name}>"


class CustomScheme(Enum):
    SSH = "ssh"
    FTP = "ftp"
    HTTP = "http"
    HTTPS = "https"

    def __str__(self):
        return f"{self.__class__.__name__}.{self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self.name}>"


class CustomMethod(Enum):
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    PATCH = "PATCH"
    DELETE = "DELETE"
    HEAD = "HEAD"
    OPTIONS = "OPTIONS"

    def __str__(self):
        return f"{self.__class__.__name__}.{self.value}"

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self.name}>"


class CustomHTTPRequest:
    def __init__(
            self, *,
            scheme: CustomScheme = None,
            web_hook: str = None,
            host: str = None,
            port: CustomPort = None,
            params: tuple[tuple[str, str], ...] = None,
            headers: dict[str, str] = None,
            data: bytes = None,
            method: CustomMethod = None):
        if scheme is None:
            scheme: CustomScheme = CustomScheme[_PROTOCOL]
        if port is None:
            port: CustomPort = CustomPort[_PROTOCOL]
        if headers is None:
            headers: dict[str, str] = dict()
        if data is None:
            data: bytes = b""
        if method is None:
            method: CustomMethod = CustomMethod.GET
        self._scheme: CustomScheme = scheme
        self._web_hook: str = web_hook
        self._host: str = host
        self._port: CustomPort = port
        self._params: tuple[tuple[str, str], ...] = params
        self._headers: dict[str, str] = headers
        self._data: bytes = data
        self._method: CustomMethod = method

    def __str__(self):
        return f"{self.__class__.__name__}: scheme = {self._scheme}, web hook = {self._web_hook}, " \
               f"host = {self._host}, post = {self._port}, params = {self._params}"

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._scheme}, {self._web_hook}, {self._host}, " \
               f"{self._port}, {self._params})>"

    def __hash__(self):
        return hash(self.url)

    @classmethod
    def __call__(cls):
        return cls()

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.url == other.url
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.url != other.url
        else:
            return NotImplemented

    def _get_params(self) -> str:
        if self._params is None:
            return ""
        _params = "&".join([f"{name}={value}" for name, value in self._params])
        return f"?{_params}"

    def _get_web_hook(self) -> str:
        if self._web_hook is None:
            print("Не указан web hook")
            raise RequiredAttributeMissingError
        return self._web_hook.rstrip("/")

    def _get_scheme(self) -> str:
        return self._scheme.value.lower().strip("/")

    def _get_port(self) -> int:
        return self._port.value

    def _get_host(self) -> str:
        if self._host is None:
            print("Не указан адрес host")
            raise RequiredAttributeMissingError
        return self._host.strip("/")

    def _get_method(self) -> str:
        return self._method.value

    @property
    def url(self) -> str:
        _scheme: str = self._get_scheme()
        _host: str = self._get_host()
        _port: int = self._get_port()
        _web_hook: str = self._get_web_hook()
        _params: str = self._get_params()
        _url: str = f"{_scheme}://{_host}:{_port}{_web_hook}{_params}"
        return quote_plus(_url, ":/&?=")

    @property
    def request(self) -> Request:
        return Request(self.url, self._data, self._headers, self._host, False, self._get_method())

    def response(self) -> HTTPResponse:
        return urlopen(self.request)


class CustomResponse:
    def __init__(
            self,
            http_response: HTTPResponse,
            path_file: Path = Path(__file__).with_name("_temp_file"),
            chunk: int = _CHUNK_SIZE):
        self._http_response: HTTPResponse = http_response
        self._path_file: Path = path_file
        self._chunk: int = chunk
        self._response_dict: dict[str, Any] = dict()

    def _exists(self) -> bool:
        return self._path_file.exists()

    def get_response(self):
        if self._exists():
            self._path_file.unlink(missing_ok=True)

        with open(self._path_file, "xb+") as fb:
            while chunk := self._http_response.read(self._chunk):
                fb.write(chunk)

        with open(self._path_file, "rb+") as fb:
            full_response: dict[str, Any] = json.loads(fb.read())

        self._response_dict = full_response

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._http_response.read().decode('utf-8')})>"

    def __str__(self):
        return self._http_response.read().decode("utf-8")

    def __hash__(self):
        return hash(self._http_response)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._http_response == other._http_response
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._http_response != other._http_response
        else:
            return NotImplemented

    def __len__(self):
        return len(self.dict_response)

    def __getitem__(self, item):
        return self._response_dict.get(item, None)

    def __bool__(self):
        return len(self) > 0

    @property
    def dict_response(self) -> dict[str, Any]:
        with open(self._path_file, "rb") as fb:
            text: dict[str, Any] = json.loads(fb.read())
        return text

    def delete_temp_file(self):
        self._path_file.unlink(missing_ok=True)


class GitlabRequest(CustomHTTPRequest):
    def __init__(self, file_name: Optional[str] = None):
        if file_name is None:
            file_name: str = _gitlab[pl]
        scheme: CustomScheme = CustomScheme.HTTPS
        web_hook: str = f"/api/v4/projects/43697703/repository/files/bin%2F{file_name}"
        host: str = "gitlab.com"
        port: CustomPort = CustomPort.HTTPS
        params: tuple[tuple[str, str], ...] = (("ref", "main"),)
        method: CustomMethod = CustomMethod.GET
        super().__init__(scheme=scheme, web_hook=web_hook, host=host, port=port, params=params, method=method)
        self._file_name: str = file_name

    @property
    def url(self) -> str:
        _url: str = f"{self._get_scheme()}://{self._get_host()}{self._web_hook}{self._get_params()}"
        return quote_plus(_url, "%/:&?=")


class GitlabResponse(CustomResponse):
    def __init__(
            self,
            http_response: HTTPResponse,
            path_file: Path = Path(__file__).with_name("_temp_file"),
            chunk: int = _CHUNK_SIZE):
        if pl not in _gitlab:
            print(f"Unknown platform {pl}")
            raise UnknownPlatformError
        super().__init__(http_response, path_file, chunk)

    def _base64_content(self) -> bytes:
        return base64.b64decode(self.dict_response.get("content"))

    @property
    def file_name(self) -> Path:
        return self._path_file.with_name(_gitlab[pl])

    def generate_executable(self):
        with open(self.file_name, "wb") as fb:
            fb.write(self._base64_content())


def main():
    gitlab_request: GitlabRequest = GitlabRequest()
    http_response: HTTPResponse = gitlab_request.response()
    desktop: Path = Path.home().joinpath("Desktop").joinpath("_temp_file")
    gitlab_response: GitlabResponse = GitlabResponse(http_response, desktop)
    gitlab_response.get_response()
    gitlab_response.generate_executable()
    print(f"Загружен файл {gitlab_response.file_name}")
    gitlab_response.delete_temp_file()
    input("Нажмите <Enter>, чтобы закрыть окно ...")


if __name__ == '__main__':
    faulthandler.enable()
    main()
