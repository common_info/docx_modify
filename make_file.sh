git clone git@gitlab.com:tech_writers_protei/docx_modify.git
cd ./docx_modify/docx_modify/

# shellcheck disable=SC1073
# shellcheck disable=SC1072
# shellcheck disable=SC1009
if [[ "$OSTYPE" in msys*) ]]; then
  python -m pip install --upgrade pyinstaller
  pyinstaller --noconfirm --onefile --console --name "docx_modify.exe" --distpath "../bin" \
  --add-data "../sources;sources/" \
  --add-data "../docs;docs/" \
  --add-data "../LICENSE;." \
  --add-data "../MANIFEST.in;." \
  --add-data "../pyproject.toml;." \
  --add-data "../README.md;." \
  --add-data "../requirements.txt;." \
  --collect-binaries "." \
  --paths "." \
  --hidden-import "loguru" --hidden-import "lxml" --hidden-import "wxPython" "./main.py"
  cd ../bin
  .\docx_modify.exe
elif [[ "$OSTYPE" in linux*) | "$OSTYPE" in darwin*) ]]; then
  python3 -m pip install --upgrade pyinstaller
  pyinstaller --noconfirm --onefile --console --name "docx_modify" --distpath "../bin" \
  --add-data "../sources:sources/" \
  --add-data "../docs:docs/" \
  --add-data "../LICENSE:." \
  --add-data "../MANIFEST.in:." \
  --add-data "../pyproject.toml:." \
  --add-data "../README.md:." \
  --add-data "../requirements.txt:." \
  --collect-binaries "." \
  --paths "." \
  --hidden-import "loguru" --hidden-import "lxml" --hidden-import "wxPython" "./main.py"
  cd ../bin
  sh docx_modify
fi
